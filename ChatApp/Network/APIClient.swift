//
//  APIClient.swift
//  ChatApp
//
//  Created by Luis Francisco Dzuryk on 14/06/2019.
//  Copyright © 2019 Luis Francisco Dzuryk. All rights reserved.
//

import Foundation
import Alamofire

protocol APIClientInterface {
    func login(user: User, success:@escaping (_ result: User) -> Void, fail: @escaping (_ error: Error) -> Void)
    func logout(user: User, success:@escaping (_ result: User) -> Void, fail: @escaping (_ error: Error) -> Void)
    func sendMessage(message: Message, success:@escaping (_ result: MessageStatusResponse) -> Void, fail: @escaping (_ error: Error) -> Void)
    func getMessages(user: User, success:@escaping (_ result: [Message]) -> Void, fail: @escaping (_ error: Error) -> Void)
    func getStatus(success:@escaping (_ result: Server) -> Void, fail: @escaping (_ error: Error) -> Void)
}

class APIClient : APIClientInterface {
    
    func login(user: User, success:@escaping (_ result: User) -> Void, fail: @escaping (_ error: Error) -> Void) {
        Alamofire.request(Router.login(user: user)).responseJSON { response in
            if response.result.isSuccess {
                let json = response.data
                do{
                    let decoder = JSONDecoder()
                    let result = try decoder.decode(User.self, from: json!)
                    success(result)
                }catch let error {
                    fail(error)
                }
            } else {
                fail(response.result.error!)
            }
        }
    }
    
    func logout(user: User, success:@escaping (_ result: User) -> Void, fail: @escaping (_ error: Error) -> Void) {
        Alamofire.request(Router.logout(user: user)).responseJSON { response in
            if response.result.isSuccess {
                let json = response.data
                do{
                    let decoder = JSONDecoder()
                    let result = try decoder.decode(User.self, from: json!)
                    success(result)
                }catch let error {
                    fail(error)
                }
            } else {
                fail(response.result.error!)
            }
        }
    }
    
    func sendMessage(message: Message, success:@escaping (_ result: MessageStatusResponse) -> Void, fail: @escaping (_ error: Error) -> Void) {
        Alamofire.request(Router.sendMessage(message: message)).responseJSON { response in
            if response.result.isSuccess {
                let json = response.data
                do{
                    let decoder = JSONDecoder()
                    let result = try decoder.decode(MessageStatusResponse.self, from: json!)
                    success(result)
                }catch let error {
                    fail(error)
                }
            } else {
                fail(response.result.error!)
            }
        }
    }
    
    func getMessages(user: User, success:@escaping (_ result: [Message]) -> Void, fail: @escaping (_ error: Error) -> Void) {
        Alamofire.request(Router.getMessages(user: user)).responseJSON { response in
            if response.result.isSuccess {
                let json = response.data
                do{
                    let decoder = JSONDecoder()
                    let result = try decoder.decode([Message].self, from: json!)
                    success(result)
                }catch let error {
                    fail(error)
                }
            } else {
                fail(response.result.error!)
            }
        }
    }
    
    func getStatus(success: @escaping (Server) -> Void, fail: @escaping (Error) -> Void) {
        Alamofire.request(Router.status).responseJSON { response in
            if response.result.isSuccess {
                let json = response.data
                do{
                    let decoder = JSONDecoder()
                    let result = try decoder.decode(Server.self, from: json!)
                    success(result)
                }catch let error {
                    fail(error)
                }
            } else {
                fail(response.result.error!)
            }
        }
    }
}
