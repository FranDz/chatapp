//
//  Router.swift
//  ChatApp
//
//  Created by Luis Francisco Dzuryk on 14/06/2019.
//  Copyright © 2019 Luis Francisco Dzuryk. All rights reserved.
//

import Foundation
import Alamofire

public enum Router: URLRequestConvertible {
    case login(user: User)
    case logout(user: User)
    case status
    case sendMessage(message: Message)
    case getMessages(user: User)
    
    
    // MARK: - Properties
    static let BaseURL: URL = URL(string: Bundle.main.infoDictionary!["BaseURL"] as! String)!
    
    var path: String {
        switch self {
        case .login(let user): return user.userId.isEmpty ? "/user/login/" : "/user/\(user.userId)/login/"
        case .logout(let user): return "/user/\(user.userId)/logout/"
        case .status: return "/server/status"
        case .sendMessage(_): return "/server/message"
        case .getMessages(let user): return "/user/\(user.userId)/message"
        }
    }
    
    var httpMethod: Alamofire.HTTPMethod {
        switch self {
        case .login(_): return .post
        case .logout(_): return .post
        case .status: return .get
        case .sendMessage(_): return .post
        case .getMessages(_): return .get
        }
    }
    
    // MARK: - URLRequestConvertible overriden properties / functions
    
    public func asURLRequest() throws -> URLRequest {
        let url: URL = Router.BaseURL.appendingPathComponent(self.path)
        var request: URLRequest = URLRequest(url: url)
        request.httpMethod = self.httpMethod.rawValue
        
        let result: URLRequest = {
            switch self {
            case .login(let user):
                let data = try? JSONEncoder().encode(user)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.httpBody = data
                return request
                
            case .logout(_):
                return request
                
            case .status:
                return request
                
            case .sendMessage(let message):
                let data = try? JSONEncoder().encode(message)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.httpBody = data
                return request
                
            case .getMessages(_):
                return request
                
            }
        }()
        return result
    }
}
