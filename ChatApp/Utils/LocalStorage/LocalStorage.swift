//
//  LocalStorage.swift
//  ChatApp
//
//  Created by Luis Francisco Dzuryk on 24/06/2019.
//  Copyright © 2019 Luis Francisco Dzuryk. All rights reserved.
//

import Foundation

class LocalStorage {
    private static let sharedUserDefaultsGroup = "group.chatapp.userDefaults"
    
    static func setDemoMode(demoMode: Bool) {
        guard let defaults = UserDefaults(suiteName: LocalStorage.sharedUserDefaultsGroup) else {
            return
        }
        defaults.setValue(demoMode, forKey: "DemoMode")
        defaults.synchronize()
    }
    
    static func getDemoMode() -> Bool {
        guard let defaults = UserDefaults(suiteName: LocalStorage.sharedUserDefaultsGroup) else {
            return false
        }
        return defaults.bool(forKey: "DemoMode")
    }
}
