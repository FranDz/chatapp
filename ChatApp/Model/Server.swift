//
//  Server.swift
//  ChatApp
//
//  Created by Luis Francisco Dzuryk on 18/06/2019.
//  Copyright © 2019 Luis Francisco Dzuryk. All rights reserved.
//

import Foundation

public enum Status: Int, Codable {
    case inactive = 0
    case active
    
    public var description: String {
        switch self {
        case .active:
            return "Server Active"
        case .inactive:
            return "Server Inactive"
        }
    }
}

class Server : Codable {
    let name: String
    let status: Status
    
    private enum CodingKeys: String, CodingKey {
        case name
        case status
    }
    
    init(name: String, status: Status) {
        self.name = name
        self.status = status
    }
}


