//
//  User.swift
//  ChatApp
//
//  Created by Luis Francisco Dzuryk on 14/06/2019.
//  Copyright © 2019 Luis Francisco Dzuryk. All rights reserved.
//

import Foundation

public struct User: Codable {
    let id: Int?
    let name: String
    var userId: String {
        get {
            guard let id = self.id else {
                return ""
            }
            return String(id)
        }
    }
    private enum CodingKeys: String, CodingKey {
        case id
        case name
    }
}
