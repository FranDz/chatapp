//
//  ChatCtrler.swift
//  ChatApp
//
//  Created by Luis Francisco Dzuryk on 17/06/2019.
//  Copyright © 2019 Luis Francisco Dzuryk. All rights reserved.
//

import Foundation

class ChatCtrler {
    private weak var view: ChatViewInterface!
    private let apiClient: APIClientInterface
    private var timer: DispatchSourceTimer?
    private let user: User
    private var showNetworkError = true
    private var status = Status.active
    
    init(_ view: ChatViewInterface, apiClient: APIClientInterface, user: User) {
        self.view = view
        self.apiClient = apiClient
        self.user = user
    }
    
    func startPolling() {
        let queue = DispatchQueue.global(qos: .background)
        timer = DispatchSource.makeTimerSource(queue: queue) as DispatchSourceTimer
        timer?.schedule(deadline: .now(), repeating: .seconds(2), leeway: .seconds(1))
        timer?.setEventHandler(handler: { [weak self] in
            guard let self = self else {
                return
            }
            
            if self.showNetworkError {
                self.apiClient.getMessages(user: self.user, success: { (messages) in
                    if messages.count > 0 {
                        self.view.showMessages(messages: messages)
                    }
                }) { (error) in
                    self.view.networkError(error: error)
                    self.showNetworkError = false
                }
            }
            
            self.apiClient.getStatus(success: { (server) in
                self.view.showStatus(server: server)
                if self.status != server.status && server.status == .inactive {
                    self.status = server.status
                    self.view.askPlayVideo()
                }
            }, fail: { (error) in
                if self.showNetworkError {
                    self.view.networkError(error: error)
                    self.showNetworkError = false
                }
            })
        })
        timer?.resume()
    }
    
    func stopPolling() {
        timer?.cancel()
        timer = nil
    }

    func sendMessage(message: Message) {
        apiClient.sendMessage(message: message, success: { (status) in
            print("SENT: \(message.message)")
        }) { (error) in
            self.view.networkError(error: error)
        }
    }
}

