//
//  ChatVC.swift
//  ChatApp
//
//  Created by Luis Francisco Dzuryk on 14/06/2019.
//  Copyright © 2019 Luis Francisco Dzuryk. All rights reserved.
//

import UIKit

protocol ChatViewInterface: class {
    func networkError(error:Error)
    func showStatus(server: Server)
    func showMessages(messages: [Message])
    func askPlayVideo()
}

class ChatVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageTextView: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var textFieldView: UIView!
    @IBOutlet weak var textFiedViewButtonConstraint: NSLayoutConstraint!
    
    private var ctrler: ChatCtrler!
    public var user: User?
    private var messages = [Message]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        tableView.separatorStyle = .none
        sendButton.setStayledButton()
        guard let user = user else {
            return
        }
        self.title = "Conecting..."
        ctrler = ChatCtrler(self, apiClient: LocalStorage.getDemoMode() ? APIDemo() : APIClient(), user: user)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ctrler.startPolling()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        ctrler.stopPolling()
    }
    
    @IBAction func sendMessageAction(_ sender: Any) {
        guard let messageText = messageTextView.text else {
            return
        }
        let message = Message(userId: 0, message: messageText, type: .sentMessage, bill: nil)
        let indexPath = IndexPath(row: messages.count, section:0)
        messages.append(message)
        messageTextView.text = ""
        tableView.insertRows(at: [indexPath], with: .right)
        tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        ctrler.sendMessage(message: message)
    }
    
    private func showPlayVideo() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "PlayVideoVC", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "PlayVideoVC") as! PlayVideoVC
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    
    @objc func keyboardWillChange(notification: Notification){
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            if notification.name == UIResponder.keyboardWillHideNotification {
                textFiedViewButtonConstraint.constant = 0
            } else {
                textFiedViewButtonConstraint.constant = keyboardHeight
            }
        }
    }
}

extension ChatVC: ChatViewInterface {
    
    func networkError(error: Error) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {
                return
            }
            let dialog = DialogViewController.dialogWithTitle(title: "Network Error", message: error.localizedDescription, cancelTitle: "Ok")
            dialog.show(inViewController: self)
        }
    }
    
    func showStatus(server: Server) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {
                return
            }
            self.title = server.name
            print(server.status.description)
        }
    }
    
    func showMessages(messages: [Message]) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {
                return
            }
            var indexPaths = [IndexPath]()
            let newCount = self.messages.count + messages.count
            for i in self.messages.count..<newCount {
                indexPaths.append(IndexPath(row: i, section:0))
            }
            self.messages.append(contentsOf: messages)
            self.tableView.insertRows(at: indexPaths, with: .left)
            if let lastIndex = indexPaths.last {
                self.tableView.scrollToRow(at: lastIndex, at: .bottom, animated: true)
            }
        }
    }
    
    func askPlayVideo() {
        let dialog = DialogViewController.dialogWithTitle(title: "Wait listening music", message: "Listen to music while waiting for the server connecting.", cancelTitle: "No")
        dialog.setActionTitle(title: "Yes please!") {
            self.showPlayVideo()
        }
        dialog.show(inViewController: self)
    }
}

extension ChatVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if messages[indexPath.row].type == .bill {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BillCell") as! BillTableViewCell
            cell.configureCell(bill: messages[indexPath.row].bill!)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell") as! MessageTableViewCell
            cell.configureCell(type: messages[indexPath.row].type, text: messages[indexPath.row].message)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.layoutSubviews()
    }
    
}

