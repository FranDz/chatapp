//
//  BillTableViewCell.swift
//  ChatApp
//
//  Created by Fernando garay on 24/06/2019.
//  Copyright © 2019 Luis Francisco Dzuryk. All rights reserved.
//

import UIKit

class BillTableViewCell: UITableViewCell {

    @IBOutlet weak var accountNumber: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var taxes: UILabel!
    @IBOutlet weak var dueDate: UILabel!
    @IBOutlet weak var totalDue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    func configureCell(bill: Bill) {
        accountNumber.text = bill.accountNumber
        productPrice.text = String(bill.price)
        taxes.text = String(bill.taxes)
        dueDate.text = bill.dueDate
        totalDue.text = String(bill.total)
    }
}
