//
//  MessageTableViewCell.swift
//  ChatApp
//
//  Created by Fernando garay on 22/06/2019.
//  Copyright © 2019 Luis Francisco Dzuryk. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    private var messageType = MessageType.sentMessage
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func configureCell(type: MessageType, text: String){
        messageLabel.text = text
        messageType = type
    }
    
    override func layoutSubviews() {
        switch messageType {
        case .recivedMessage:
            backView.backgroundColor = .gray
            backView.roundCorners(corners: [.topLeft, .topRight, .bottomLeft], radius: 8)
        case .sentMessage:
            backView.backgroundColor = .lightGray
            backView.roundCorners(corners: [.topLeft, .topRight, .bottomRight], radius:8)
        default:
            return
        }
        super.layoutSubviews()
    }
}
