//
//  Login.swift
//  ChatApp
//
//  Created by Luis Francisco Dzuryk on 14/06/2019.
//  Copyright © 2019 Luis Francisco Dzuryk. All rights reserved.
//

import UIKit

protocol LoginViewInterface: class {
    func networkError(error:Error)
    func login(user: User)
}

class LoginVC: UIViewController {
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var demoModeSwitch: UISwitch!
    
    private var ctrler: LoginCtrler!
    private var user: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginButton.setStayledButton()
        hideKeyboardWhenTappedAround()
        ctrler = LoginCtrler(self, apiClient: LocalStorage.getDemoMode() ? APIDemo() : APIClient())
        demoModeSwitch.isOn = LocalStorage.getDemoMode()
    }
    
    @IBAction func loginAction(_ sender: Any) {
        guard let userName = userNameTextField.text, !userName.isEmpty else {
            userNameTextField.setAlertStyle(text: "Please enter a user Name")
            userNameTextField.setNeedsLayout()
            userNameTextField.layoutIfNeeded()
            userNameTextField.becomeFirstResponder()
            return
        }
        
        ctrler.login(userName: userName)
    }

    @IBAction func enableDemoModeAction(_ sender: Any) {
        LocalStorage.setDemoMode(demoMode: demoModeSwitch.isOn)
        ctrler = LoginCtrler(self, apiClient: demoModeSwitch.isOn ? APIDemo() : APIClient())
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showChatVC" {
            let nextViewController = segue.destination as! UINavigationController
            let vc = nextViewController.viewControllers.first as! ChatVC
            vc.user = user
        }
    }
}

extension LoginVC: LoginViewInterface {
    func networkError(error: Error) {
        print(error)
        let dialog = DialogViewController.dialogWithTitle(title: "Network Error", message: error.localizedDescription, cancelTitle: "Ok")
        dialog.show(inViewController: self)
    }
    
    func login(user: User) {
        print(user.name + " " + user.userId)
        self.user = user
        performSegue(withIdentifier: "showChatVC", sender: self)
    }
}

class LoginCtrler {
    private weak var view: LoginViewInterface!
    private let apiClient: APIClientInterface
    
    init(_ view: LoginViewInterface, apiClient: APIClientInterface) {
        self.view = view
        self.apiClient = apiClient
    }
    
    func login(userName: String) {
        let user = User(id: nil, name: userName)
        apiClient.login(user: user, success: { [weak self] (user) in
            guard let self = self else {
                return
            }
            self.view.login(user: user)
        }) { (error) in
            self.view.networkError(error: error)
        }
    }
}


